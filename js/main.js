function modalBoxOUT(id,iddest){
      //id:#modalBoxed iddest: #modalBoxBG
  		$(id).removeClass("bounceInUp").addClass("bounceOutDown");
  		$("html,body").css("overflow","auto");
  		  		setTimeout(function(){
  		  			  $(iddest).removeClass("opacityIN").addClass("opacityOUT");
  		  		},800);
  		  		setTimeout(function(){
  		  			      $(iddest).css({"top":"150vh"});
  		  		},1300);

      console.log("id:"+ id +" iddest: "+ iddest);
  	}
  	function modalBoxIN(id,iddest){
      //id:#modalBoxed iddest: #modalBoxBG
      $(id).css({"top":"25vh"});
      $(iddest).css({"top":"0vh"});

  		setTimeout(function(){
  			$(id).removeClass("bounceOutDown").addClass("bounceInUp");
  		},700);
  		setTimeout(function(){
        $(id).removeClass("opacityOUT").addClass("opacityIN");
              $(iddest).removeClass("opacityOUT").addClass("opacityIN");
  		},400);
  		setTimeout(function(){
  			$("html,body").css("overflow","hidden");
  		},1300);
  	      console.log("id:"+ id +" iddest: "+ iddest);
  	}
  	function setUnSelected(){
  		$( ".lilisted" ).removeClass("liSelected").removeClass("liNotSelected");
  		  	$( ".lilisted>h2" ).css({"text-align":"center","margin-top":"","width":"","opacity":"1","margin-letf":"","opacity":"1"}).removeClass("h2reset").removeClass("h2notSelected");
  		  	$( ".lilisted>p" ).css({"display":"block"});
  		  	$( ".lilisted>section" ).removeClass("descriptionContentIN").addClass("descriptionContentOUT");
  		  	$( ".tbimg" ).removeClass("opacityIN").addClass("opacityOUT");
  		  	$( ".text-block-li" ).removeClass("opacityIN").addClass("opacityOUT");
  		  	$( ".a-block-li" ).removeClass("opacityIN").addClass("opacityOUT");
			$( ".imgClose" ).removeClass("opacityINa").addClass("opacityOUTa");
			$("#focused").val("no");
			$(".lilisted").css({"cursor":"pointer"});
			$(".moreICO").css({"display":""});

			  //$( ".lilisted>h2" ).css({"text-align":"left","margin-top":"150px","width":"120%"}).removeClass("h2notSelected").addClass("h2reset");

  		  	/*$( ".lilisted" ).removeAttr( 'style' );*/
  	}
  	function setSelected(idsent){
  			var focused=$("#focused").val();
  			var idcompare="#"+idsent;
	  			if (focused!=idcompare) {
					const tab= "#tb"; // ID for tabs
					var listItems = $(tab).children();
					var count = listItems.length;
					var w= $(tab).width();
					var wUnselected=(10*w)/100;
					var wSelected=(60*w)/100;
					var idsent="#"+idsent;

					$( ".tbimg" ).removeClass("opacityIN").addClass("opacityOUT");
					$( ".imgClose" ).removeClass("opacityINa").addClass("opacityOUTa");
					$( ".a-block-li" ).removeClass("opacityIN").addClass("opacityOUT");
					$( ".text-block-li" ).removeClass("opacityIN").addClass("opacityOUT");
					$( ".lilisted" ).removeClass("liSelected").addClass("liNotSelected");
					$( ".lilisted" ).attr('data-focus', 'notSelected');
					$( ".lilisted>h2" ).css({"text-align":"left","margin-top":"150px","width":"150%","opacity":"1"}).removeClass("h2reset").addClass("h2notSelected");
					$( ".lilisted" ).css({"cursor":"pointer"});
					$( ".lilisted>section" ).removeClass("descriptionContentIN").addClass("descriptionContentOUT");
					$(".moreICO").css({"display":"none"});
					$( ".lilisted>p" ).css({"display":"none"});
					$(idsent).removeClass("liNotSelected").addClass("liSelected");
					$("#focused").val(idsent);
					$(idsent+">h2").css({"opacity":"0"});
					$(idsent).css({"cursor":"default"});
					$(idsent+ "> img" ).removeClass("opacityOUTa").addClass("opacityINa");
					$( idsent+">section" ).removeClass("descriptionContentOUT").addClass("descriptionContentIN");
					setTimeout(function(){
						$( idsent + " > section > div:nth-child(2) > div:nth-child(1) > div > .tbimg" ).removeClass("opacityOUT").addClass("opacityIN");
					},200);
					setTimeout(function(){
						$( idsent + " > section > div:nth-child(2) > div.col-1-2.text-block > div" ).removeClass("opacityOUT").addClass("opacityIN");
					},400);
					setTimeout(function(){
						$( idsent + " > section > div:nth-child(2) > div.col-1-2.text-block > a" ).removeClass("opacityOUT").addClass("opacityIN");
					},600);
				}else{
					setUnSelected();
				}
	}
  	$(document).ready(function(){
  		  	//console.log('Ready to GO!');
  		  	//var widthdoc = $("#S4success").width();
  		  	//var sliderw30=Math.round(widthdoc*0.35);
  		  	//var sliderw10=Math.round(widthdoc*0.10);
  		  	//console.log(widthdoc + " "+ sliderw30);
          //$("#tb").attr("disabled", "disabled");
  	      	//$('.slider').bxSlider({  auto: true,tickerHover:true,mode:'horizontal',speed:1000,pause:8000,onSlideAfter: function($slideElement) {
        //console.log('onSlideAfter'); //not executed if useCSS: true
    //}});
  	      //$('.bxsliderStories').bxSlider({  auto: true,tickerHover:true,speed:1000,pause:5000,controls: false,slideWidth: sliderw30});
  		  /*	$("#modalBoxBTN").click(function(){
  		  		modalBoxIN("#modalBoxed","#modalBoxBG");
  		  	});
  		  	$("#modalBoxBG").click(function(){
  		  		modalBoxOUT("#modalBoxed","#modalBoxBG");
  		  	});*/
  	});