const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
//const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');
const uncss = require('gulp-uncss');
const csso = require('gulp-csso');
const gulpSequence = require('gulp-sequence');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const w3cjs = require('gulp-w3cjs');
const through2 = require('through2');
const removeHtmlComments = require('gulp-remove-html-comments');
const deleteUnusedImages = require('gulp-delete-unused-images');
const plumber = require('gulp-plumber');
const htmlmin = require('gulp-html-minifier');


/* DIST ENVIRONMENT*/

gulp.task('dist:minify:JS', function () {
  return gulp.src('./js/main.js')
  .pipe(concat('main.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./minified/js/'))
});

gulp.task('dist:autoprefixer:CSS', () =>
    gulp.src('./css/main.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./tmp/dist/prefixed'))
);

gulp.task('dist:minify:CSS', function () {
  return gulp.src('./tmp/dist/prefixed/main.css')
    .pipe(concat('main.css'))
    .pipe(cleanCSS({compatibility: 'ie8',debug: true},function(details){
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(gulp.dest('./minified/css/'));
});


gulp.task('dist:minify:remove-html-comments-HTML', function () {
  return gulp.src('./tmp/**/*.html')
    .pipe(removeHtmlComments())
    .pipe(gulp.dest('./minified'));
});


gulp.task('dist:minify:HTML', function() {
  gulp.src('./*.html')
    .pipe(htmlmin({collapseWhitespace: true,removeComments:true,minifyJS:true,ignorePath: './node_modules'}))
    .pipe(gulp.dest('./minified'))
});

gulp.task('dist:minify:HTML-printers-scanners', function() {
  gulp.src('./printers-scanners/*.html')
    .pipe(htmlmin({collapseWhitespace: true,removeComments:true,minifyJS:true}))
    .pipe(gulp.dest('./minified/printers-scanners'))
});


gulp.task('dist:minify:HTML-laptops', function() {
  gulp.src('./laptops-tablets/*.html')
    .pipe(htmlmin({collapseWhitespace: true,removeComments:true,minifyJS:true}))
    .pipe(gulp.dest('./minified/laptops-tablets'))
});

gulp.task('dist:minify:HTML-desktops', function() {
  gulp.src('./desktops/*.html')
    .pipe(htmlmin({collapseWhitespace: true,removeComments:true,minifyJS:true}))
    .pipe(gulp.dest('./minified/desktops'))
});

gulp.task('dist:delete-unussed:IMG', function() {
      gulp.src(['./uploads/images/*', './**/*.html','./**/*.css'])
      .pipe(plumber())
      .pipe(deleteUnusedImages({
        log: true,
        delete: false
      }))
      .pipe(gulp.dest('./minified/uploads/images'));
});
gulp.task('dist:copy:IMG', function() {
    gulp.src(['./uploads/images/*']).pipe(gulp.dest('./minified/uploads/images'));
});
gulp.task('dist:bckp:IMG', function() {
    gulp.src(['./uploads/images/*']).pipe(gulp.dest('./uploadsBCKP/images'));
});


gulp.task('dist:compress:IMG', () =>
    gulp.src('./uploads/images/*')
        .pipe(imagemin([
          imagemin.gifsicle({interlaced: true}),
          imagemin.jpegtran({progressive: true,optimizationLevel: 8,interlaced: true}),
          imagemin.optipng({optimizationLevel: 8,interlaced: true}),
          imagemin.svgo({plugins: [{removeViewBox: true}]})
]))
        .pipe(gulp.dest('./minified/uploads/images'))
);

gulp.task('dist:copy:FONTS', function() {
    gulp.src(['./css/fonts/*']).pipe(gulp.dest('./minified/css/fonts'));
});

gulp.task('dist:copy:IMG', function() {
    gulp.src(['./uploads/images/*']).pipe(gulp.dest('./minified/uploads/images'));
});
gulp.task('dist:bckp:IMG', function() {
    gulp.src(['./uploads/images/*']).pipe(gulp.dest('./uploadsBCKP/images'));
});

gulp.task('dist:copy:favicon', function() {
    gulp.src(['./favicon.ico']).pipe(gulp.dest('./minified'));
});



gulp.task('clear:ALL', function () {
    return gulp.src('./minified', {read: false})
        .pipe(clean());
})
gulp.task('clear:TMP', function () {
    return gulp.src('./tmp', {read: false})
        .pipe(clean());
})


gulp.task('dist', gulpSequence(['clear:TMP','clear:ALL']));
gulp.task('min', gulpSequence(['dist:minify:JS','dist:minify:HTML','dist:minify:HTML-printers-scanners','dist:minify:HTML-laptops','dist:minify:HTML-desktops','dist:autoprefixer:CSS'],['dist:minify:CSS'],['dist:copy:FONTS','dist:copy:favicon']));
gulp.task('img', gulpSequence(['dist:compress:IMG']));

//gulp.task('build', gulpSequence(['dist:compress:IMG']));


gulp.task('default', gulpSequence(['dist'],['min'],['img'],['clear:TMP']));